# WordPress - Odoo integration

Simple plugin to test WordPress - Odoo integration via XML-RCP.

Odoo is open source ERP and CRM, more here: https://www.odoo.com/

WordPress is open source CMS, more here: https://wordpress.com/

This is a simple proof of concept to check if integration is possible.


## Requirements

PHP XML-RPC client installed in  <plugin_folder>/includes/phpxmlrpc/ folder.

Tested with XMLRPC for PHP:
https://github.com/gggeek/phpxmlrpc

Simply clone or download it to  <plugin_folder>/includes/phpxmlrpc/ folder.
```
    cd <plugin_folder>/includes
    git clone https://github.com/gggeek/phpxmlrpc.git
```
## Installation

Copy plugin (along with XML-RPC lib) to your /wp-content/plugins/ directory.

Find *odoo-integration* plugin among installed plugins and activate it.

Upon activation navigate to *'Odoo settings'* and enter required values: URI, database, user and password. Those are values that will be used to connect to Odoo.

## Testing

Navigate browser to `/res_currency/`, e.g. https://<your_domain>/res_currency/ or `/res_country/`, e.g. https://<your_domain>/res_country/ 

If everything is setup correctly you should see a list of currencies or countries respectively.

## Structure

* includes/
  * OdooBase.php
  * OdooIntegration.php
  * OdooIntegrationSettings.php
* templates/
  * rows-table.php
* odoo-integration.php

`OdooBase.php` contains `OdooBase` class that is responsible for all XML-RPC heavy lifting.

`OdooIntegration.php` contains `OdooIntegration` class that binds Odoo and WordPress together

`OdooIntegrationSettings.php` contains `OdooIntegrationSettings` class that will display settings page on WordPress backend

`rows-table.php` is template used to display any structured array as HTML table

`odoo-integration.php` is plugin's main file used to load `OdooIntegration` and `OdooIntegrationSettings` if needed.

## Notes

This is example of possible Odoo - WordPress integration. It is stripped of some plugin boilerplate code and by no means finished. Some things could (and probably should) be done differently but this is merely proof of concept that it is possible to use WordPress as a front end for Odoo ERP capabilities.

Admin area is as simple as it gets and there are things that should be implemented before production, particulary in security area.

Public faced side currenly displays list of all values in Odoo's `res_country` or `res.currency` table as an example. This could be easily extended to any table or to support any XML-RPC call. Some caching mechanism would be nice to avoid hammering Odoo too much with XML-RPC requests.

XML-RPC on it's own is a tricky beast so sprinkling few sanity checks here and there wouldn't hurt.

This is **not** production ready plugin as it is intended mostly for research. Use at your own risk.