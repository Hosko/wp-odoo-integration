<?php
/**
 * create HTML table of provided $rows
 */ 
function render_rows_list($rows) {
?>
  <div class="custom-list">
    <?php
    if (count($rows) == 0) {
      echo "<p>Empty dataset!</p>";
    } else {
      echo "<table>";
      // create table header
      echo "<tr>";
      foreach ($rows[0] as $key => $val) {
        echo "<th>" . $key . "</th>";
      }
      echo "</tr>";

      // fill table data
      foreach ($rows as $row) {
        echo "<tr>";
        foreach ($row as $key => $val) {
          echo "<td>" . $val . "</td>";
        }
        echo "</tr>";
      }
      echo "</table>";
    }
    ?>
  </div>
<?php
}
