<?php

require_once 'OdooBase.php';

/**
 * Main class of plugin. Loads OdooBase and waits for incoming requests.
 * If REQUEST_URI matches given pattern new WordPress page is generated with content provided by Odoo.
 */ 
class OdooIntegration {

  private $odooBase;
  
  /**
   * Check if URL cointains given string.
   * 
   * @param string $uri_segment search string 
   * @return bool true if URL contains given string.
   */ 
  function check_application_url($uri_segment) {
    return false !== strpos(strtolower($_SERVER['REQUEST_URI']), $uri_segment);
  }

  /**
   * Create new page with title and content
   * 
   * @param string $title Title of page
   * @param string $content Content of page
   * 
   * @return Array list of posts containing singe post with given title and content.
   */ 
  function createDummyPage($title, $content) {
    $posts = null;
    $post = new stdClass();
    $post->post_content = $content;
    $post->post_title = $title;
    $post->post_type = "page";
    $post->comment_status = "closed";
    $posts[] = $post;
    return $posts;
  }

  /**
   * Return HTML content for countries returned from XML-RPC
   * 
   * @return string HTML with list of all countries.
   */ 
  function displayRowsTable($rows) {
    // render html content and return it for post content
    include_once ODOOWP_DIR.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.'rows-table.php';
    ob_start();
    render_rows_list( $rows );
    return ob_get_clean();
  }


  /**
   * Get HTML of XML-RPC response and create new page.
   *
   * @return Array list of posts containing singe post with list of countries.
   */ 
  function createResCountryPost() {
    $countries = $this->odooBase->read_from_table('res.country', ["id", "name", "code"], []);
    return $this->createDummyPage("List of countries", $this->displayRowsTable($countries));
  }

  function createResCurrencyPost() {
    $currencies = $this->odooBase->read_from_table('res.currency', ["id", "name", "symbol"], []);
    return $this->createDummyPage("List of currencies", $this->displayRowsTable($currencies));
  }

  /**
   * Check incoming request
   */ 
  function url_check_rules() {
    if ($this->check_application_url('/res_country/')) {
      // inject custom post if REQUEST_URI matches
      add_filter('the_posts', array($this, 'createResCountryPost'));
    }
    if ($this->check_application_url('/res_currency/')) {
      // inject custom post if REQUEST_URI matches
      add_filter('the_posts', array($this, 'createResCurrencyPost'));
    }
  }

  /**
   * run application
   */ 
  function run() {
    $uri = get_option('uri');
    $database = get_option('database');
    $password = get_option('password');
    $user = get_option('user');
    $uid = get_option('odoo_uid');
    $this->odooBase = new OdooBase($uri, $database, $password, $user, $uid);
    // add URL checking rules
    add_action('init', array($this, 'url_check_rules'));
  }
}
