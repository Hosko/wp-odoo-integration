<?php
require_once 'OdooBase.php';

class OdooIntegrationSettings {
  public function __construct() {
    add_action('admin_menu', array($this, 'odoo_wp_create_menu'));
    add_action( 'admin_notices', array( $this, 'admin_notices_odoo_success' ) );
  }

  function odoo_wp_create_menu() {

    //create new top-level menu
    add_menu_page('Odoo settings', 'Odoo settings', 'administrator', __FILE__, array($this, 'odoo_wp_settings_page'));

    //call register settings function
    add_action('admin_init', array($this, 'register_odoo_wp_plugin_settings'));
  }

  function admin_notices_odoo_success() {
    $screen = get_current_screen();
    if($screen->parent_base == 'odoo-integration/includes/OdooIntegrationSettings') {
      if ( isset( $_GET['settings-updated'] ) ) {
            //if settings updated successfully 
            if ( 'true' === $_GET['settings-updated'] ) {
                    $this->check_odoo_connection();
            }
      }
    }
  }

  function check_odoo_connection() {
    $uri = get_option('uri');
    $database = get_option('database');
    $password = get_option('password');
    $user = get_option('user');

    $instance = new OdooBase($uri, $database, $password, $user, false);
    $uid = $instance->connect();
    if ($uid != 0) {
      if ( get_option( 'odoo_uid' ) !== false ) {
          update_option( 'odoo_uid', $uid );
      } else {
          add_option( 'odoo_uid', $uid );
      }


      echo '<div class="notice notice-success is-dismissible"><p>';
      echo 'Connection successfull, UID: '. $uid . "\n";
      echo '</p></div>';
    } else {
      echo '<div class="notice notice-error is-dismissible"><p>';
      echo 'Something went wrong! Check your username or password!'. "\n";
      echo '</p></div>';
    }
}

  function register_odoo_wp_plugin_settings() {
    //register our settings
    register_setting('odoo_integration_settings_group', 'uri');
    register_setting('odoo_integration_settings_group', 'database');
    register_setting('odoo_integration_settings_group', 'user');
    register_setting('odoo_integration_settings_group', 'password');
  }

  function odoo_wp_settings_page() {
?>
    <div class="wrap">
      <h1>Odoo settings</h1>
      <form method="post" action="options.php">
        <?php settings_fields('odoo_integration_settings_group'); ?>
        <?php do_settings_sections('odoo_integration_settings_group'); ?>
        <table class="form-table">
          <tr valign="top">
            <th scope="row">URI</th>
            <td><input type="text" name="uri" value="<?php echo esc_attr(get_option('uri')); ?>" /></td>
          </tr>
          <tr valign="top">
            <th scope="row">Database</th>
            <td><input type="text" name="database" value="<?php echo esc_attr(get_option('database')); ?>" /></td>
          </tr>
          <tr valign="top">
            <th scope="row">User</th>
            <td><input type="text" name="user" value="<?php echo esc_attr(get_option('user')); ?>" /></td>
          </tr>

          <tr valign="top">
            <th scope="row">Password</th>
            <td><input type="text" name="password" value="<?php echo esc_attr(get_option('password')); ?>" /></td>
          </tr>
        </table>

        <?php submit_button(); ?>
      </form>
    </div>
<?php
  }
}
