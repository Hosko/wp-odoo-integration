<?php
// include and wrap XML-RPC library and classes
// tested with phpxmlrpc installed in  <plugin_folder>/includes/phpxmlrpc/:
// https://github.com/gggeek/phpxmlrpc
// stand-alone libraries are easy to deploy
// make sure that <plugin_folder>/includes/phpxmlrpc/lib/xmlrpc.inc exists

include(ODOOWP_DIR.DIRECTORY_SEPARATOR."includes". DIRECTORY_SEPARATOR ."phpxmlrpc". DIRECTORY_SEPARATOR ."lib". DIRECTORY_SEPARATOR ."xmlrpc.inc");

// set internal coding to 'UTF-8' to avoid scrambled results
PhpXmlRpc\PhpXmlRpc::$xmlrpc_internalencoding = 'UTF-8';

function wrap_xmlrpcval($val, $type) {
  return new xmlrpcval($val, $type);
}

function wrap_xmlrpc_client($url) {
  return new xmlrpc_client($url);
}

function wrap_xmlrpcmsg($msg) {
  return new xmlrpcmsg($msg);
}



/**
 * Base class for Odoo connection. 
 * Handles connection, authentication, search and read from Odoo's XML-RPC.
 */ 
class OdooBase {
  protected $uri; // Odoo instance URL, e.g. "https://example.com"
  protected $user; // Odoo instance XML-RPC user
  protected $password; // Odoo instance XML-RPC password
  protected $database; // Odoo instance XML-RPC database
  protected $uid; // uid that will be used on client calls to Odoo

  public function __construct($uri, $database, $password, $user, $uid) {

    $this->uri = $uri;
    $this->database = $database;
    $this->password = $password;
    $this->user = $user;

    if ($uid) {
      $this->uid = $uid;
    } else {
      $this->uid = $this->connect();
    }

    // obtain client for executing calls
    $this->client = wrap_xmlrpc_client($this->uri . "/xmlrpc/2/object");

  }

  /**
   * Connect to Odoo, authorise and obtain uid that will be used in subsequent XML-RPC calls.
   * 
   * @return int uid
   */ 
  function connect() {
    if ($this->uid) {
      return $this->uid;
    }

    try {
      $loginConnection = wrap_xmlrpc_client($this->uri . "/xmlrpc/2/common");
      //$loginConnection->setSSLVerifyPeer(0);
    } catch (Exception $e) {
      return false;
    }

    $loginMessage = wrap_xmlrpcmsg('login');
    $loginMessage->addParam(wrap_xmlrpcval($this->database, "string"));
    $loginMessage->addParam(wrap_xmlrpcval($this->user, "string"));
    $loginMessage->addParam(wrap_xmlrpcval($this->password, "string"));
    try {
      $loginResponse = $loginConnection->send($loginMessage);
    } catch (Exception $e) {
      return false;
    }
    if ($loginResponse->errno != 0) {
      return false;
    }
    $this->uid = $loginResponse->value()->scalarval();
    return $this->uid;
  }

  /**
   * Creates 'execute' XML-RPC message and appends needed credentials.
   * 
   * @return xmlrpcmsg xmlrpcmsg message.
   */ 
  function getAuthMsg() {
    if (!isset($this->uid)) {
      $this->connect();
    }

    $msg = wrap_xmlrpcmsg('execute');
    $msg->addParam(wrap_xmlrpcval($this->database, "string"));
    $msg->addParam(wrap_xmlrpcval($this->uid, "int"));
    $msg->addParam(wrap_xmlrpcval($this->password, "string"));
    return $msg;
  }

  /**
   * Search for records on Odoo via XML-RPC
   * 
   * @param string $table table to search
   * @param Array $domain_filter xmlrpcval domain filter 
   * @return Array list of ids that match given domain filter.
   */ 
  function search($table, $domain_filter) {
    $msg = $this->getAuthMsg();

    $msg->addParam(wrap_xmlrpcval($table, "string"));
    $msg->addParam(wrap_xmlrpcval("search", "string"));
    $msg->addParam(wrap_xmlrpcval($domain_filter, "array"));

    try {
      $response = $this->client->send($msg);
    } catch (Exception $e) {
      return [];
    }
    if ($response->faultCode()) {
      return [];
    }

    return $response->value();
  }

  /**
   * Read given fields on records from Odoo table via XML-RPC.
   * 
   * @param string $table table to read from
   * @param Array $id_list list of ids to filter
   * @param Array $field_list xmlrpcval list of fields to read
   * @return Array XML with result data.
   */ 
  function read($table, $id_list, $field_list) {
    $msg = $this->getAuthMsg();

    $msg->addParam(wrap_xmlrpcval($table, "string"));
    $msg->addParam(wrap_xmlrpcval("read", "string"));
    $msg->addParam(wrap_xmlrpcval($id_list, "array"));
    $msg->addParam(wrap_xmlrpcval($field_list, "array"));

    try {
      $response = $this->client->send($msg);
      if ($response->faultCode()) {
        return [];
      }

      $result = $response->value()->scalarval();
    } catch (Exception $e) {
      return [];
    }

    return $result;
  }

  /**
   * Combine search and read, return data as simple array.
   * 
   * @param string $table table to search e.g. "res.country"
   * @param Array $fields list of fields to read e.g. ["id", "name"]
   * @param Array $domain_filter xmlrpcval domain filter 
   * @return Array result data.
   */ 
  function read_from_table($table, $fields, $domain_filter) {
    $retArray = [];
    $field_list = array();

    // construct a list of fields to be used in read call
    foreach($fields as $f) {
      $field_list[] = wrap_xmlrpcval($f, "string");
    }

    // seach via XML-RCP
    $ids = $this->search($table, $domain_filter);
    if ($ids) {
      // if search yielded some ids we'll use them to read actual values
      $result = $this->read($table, $ids, $field_list);
      if (count($result) > 0) {
        for ($i = 0; $i < count($result); $i++) {
          $newElement = [];
          $xmlEl = $result[$i]->me['struct'];
          // extract all values of requested fields for given record XML
          if (isset($xmlEl)) {
            foreach($fields as $f) {
              if (isset($xmlEl[$f])) {
                $newElement[$f] = $xmlEl[$f][0];
              }
            }
          }

          $retArray[] = $newElement;
        }
        return $retArray;
      }
    }

    return $retArray;
  }

}
