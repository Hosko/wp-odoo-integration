<?php
/**
 * Plugin Name: odoo-integration
 * Plugin URI: https://gitlab.com/Hosko/wp-odoo-integration
 * Description: 
 * Version: 1.0
 * Author: Ivica Hoško
 * Author URI: https://gitlab.com/Hosko
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'ODOOWP_DIR', dirname( __FILE__ ) ); // our directory

require_once(ODOOWP_DIR . DIRECTORY_SEPARATOR . 'includes'. DIRECTORY_SEPARATOR .'OdooIntegration.php');
function run_odoo_integration() {
	$plugin = new OdooIntegration();
	$plugin->run();
}

if( is_admin() ) {
	require_once(ODOOWP_DIR . DIRECTORY_SEPARATOR . 'includes'. DIRECTORY_SEPARATOR .'OdooIntegrationSettings.php');
	$odoo_settings_page = new OdooIntegrationSettings();
}

run_odoo_integration();
